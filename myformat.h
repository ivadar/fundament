#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "myfunc.h"
#include "mychange.h"

int parse(char* str, char* cbase, char* numbers, char* operators);
int get_numbers(char str[], char *numbers[], char cbase[]);
int check_base(char number[], int len, int radix);
char *trans_from_radix_to_dec(char number[], int len, char radix[]);
char *trans_from_dec_to_radix(char number[], int len, char cradix[], int len_cradix);
void func_com(size_t read, char *line);

enum states { SPC, DIG, OPR };
