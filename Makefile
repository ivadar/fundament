CFLAGS = -Wall -g
CC = gcc
SRC=$(wildcard *.o)

.PHONY: compile clean

compile: main.o myfunc.o myformat.o mychange.o user.o
	$(CC) -g $^ -o LICal -lm

main.o: main.c mychange.h myfunc.h myformat.h
	$(CC) $(CFLAGS) -c main.c
myfunc.o: mychange.c mychange.h myfunc.c myfunc.h myformat.c myformat.h
	$(CC) $(CFLAGS) -c myfunc.c
myformat.o: myfunc.c myfunc.h myformat.c myformat.h
	$(CC) $(CFLAGS) -c myformat.c
mychange.o: mychange.c mychange.h
	$(CC) $(CFLAGS) -c mychange.c
user.o: user.h
	$(CC) $(CFLAGS) -c user.c
clean:
	rm -f *.o LICal