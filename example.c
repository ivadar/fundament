//#include "big_calc.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#define SIZE 100
#define FILENAME "data/big_calc.txt"

char *delChar(char temp[], char del);
char *func_minus(char temp_one[], int len_one, char temp_two[], int len_two);
char *func_plus(char temp_one[], int len_one, char temp_two[], int len_two);
char *func_del(char temp_one[], int len_one, char temp_two[], int len_two);
char *add_zero(char temp[], int len_one, int len);

int check_base(char number[], int len, int radix) // передаем указатель на первый элемент строки, длину, основание сс
{
    int i, num_curr;
    for (i = 0; i < len; i++)
    {
    
        if (isdigit(*(number + i)) && ((*(number + i)) - '0' < radix))   continue; // если число и меньше основания (94 - ^), продолжаем

        if (isalpha(*(number + i)))    num_curr = toupper(*(number + i)) - 'A' + 10; // если буквы, то считаем и проверяем на меньше или больше
        
        else   // если не буква и цифра, говорим ошибся малец
        {
            printf("Error, invalid character\n");
            return 0;
        }
        if (num_curr >= radix)
        {
            printf("Error, number greater than base\n");
            return 0;
        }
    }
    return 1;
}

char *trans_from_radix_to_dec(char number[], int len, int radix) // перевод числа из сс в 10
{
    unsigned long long i, pow1=radix, sum = 0;
    int sign = 1;
    char *answer = strdup(number);
    if (*answer == '-')
    {
        sign = -1;
        len--;
        delChar(answer, '-');
    }
    if(radix >= 2 && radix <= 36)
    {
        if (check_base(answer, len, radix) == 1)
        {
            for (i = 0; i < len; i++)
            {
                if (isalpha(*(answer + len - 1 - i)) != 0)
                {
                    pow1 = radix*pow1;
                    if (i == 0 ) pow1 = 1;
                    sum = (toupper(*(answer + len - 1 - i)) - 'A' + 10) * pow1 + sum;
                    // sum = (toupper(*(answer + len - 1 - i)) - 'A' + 10) * pow(radix, i) + sum;
                }
                else
                { 
                    pow1 = radix*pow1;
                    if (i == 0 ) pow1 = 1;
                    sum = (*(answer + len - 1 - i) - '0') * pow1 + sum;
                    // sum = (*(answer + len - 1 - i) - '0') * pow(radix, i) + sum;
                }
            }
        }        
    }
    else 
    {
        printf("Error, base outside [2; 36]\n");
        // return EXIT_FAILURE;
    }

    for (i = 0; i < len; i++)
            delChar(answer, *(answer + i));
    
    int length = 0;
    unsigned long long sum1 = sum;
    while (sum1 > 0)
    {
        length++;
        sum1 /=10;
    }

    snprintf(answer, length+1, "%llu", sum);

    if (sign == -1)
    {
        memmove(answer + 1, answer, len+1);
        memset(answer, '-', 1);  
        return answer; 
    }
    else 
        return answer; 
}

char *delChar(char temp[], char del) // знак минус удаляет из строки
{
    for (char *c = temp; *c; c++)
    {
        while(*c == del)
            memmove(c, c + 1, strlen(c) + 1);
        if (*c == 0) break;
    }
    return temp;
}

char *add_zero(char temp[], int len_one, int len) // функция добавления 0, на ввод строку, которую надо изменить, длину этой строки, и сколько нулей добавить
{
    int i, plus = len;
    memmove(temp + plus, temp, len_one + 1);
    memset(temp, '0', plus);
    return(temp);
}

enum states { SPC, DIG, OPR, VAR, STOP, ERR };

int parse(char* str, char* cbase, char* numbers, char* operators)
/* Фунцкия разбора строки на операторы и операнды. */
{
    const char* copy = str;
    int oc = 0,nc = 0, nextstate, bracket = 0, curstate = SPC;
    short int base = 0;
    do
    {
        switch (*copy)
        {
            case ' ': case '\n': case '\0':                         // Пропускаем пробелы
                continue;
            case '(': case ')':                  // Всё что в скобках это база системы счистления
                nextstate = SPC;
                base++;
                bracket++;
                break;
            case '+': case'-': case'*': case'/': case'%': case'^':
                nextstate = OPR;
                if(curstate == SPC || curstate == OPR)
                    strncat(numbers, copy, 1);   // Если оператор в начале или после другого оператора, то это знак числа
                else
                {
                    strncat(operators, copy, 1); // Иначе записываем оператор в operators
                    strcat(numbers, ",");        // И разделитель в numbers
                }
                break;
            case '0': case '1': case '2': case '3':
			case '4': case '5': case '6': case '7':
			case '8': case '9': case 'a': case 'b': 
            case 'c': case 'd': case 'e': case 'f': 
			case 'i': case 'j': case 'k': case 'l':
			case 'm': case 'n': case 'o': case 'p':
			case 'q': case 'r': case 's': case 't':
			case 'u': case 'v': case 'w': case 'x':
			case 'y': case 'z': case 'g': case 'h':
            case 'A': case 'B': 
            case 'C': case 'D': case 'E': case 'F': 
			case 'I': case 'J': case 'K': case 'L':
			case 'M': case 'N': case 'O': case 'P':
			case 'Q': case 'R': case 'S': case 'T':
			case 'U': case 'V': case 'W': case 'X':
			case 'Y': case 'Z': case 'G': case 'H':
            nextstate = DIG;
                if(base == 1)
                {
                    strncat(cbase, copy, 1); 
                    if ((strlen(cbase) > 2) && (bracket <= 1))
                    {
                        printf("Error, you didn't put a parenthesis\n");
                        return 2;
                    }
                }
                else
                    strncat(numbers, copy, 1);
                break;
            default:
                printf("Error, you entered an invalid character - %s\n", copy);
                return 1;
                break;
        }
        curstate = nextstate;
    } while(*copy++);
    return 0;
}

long int func_compar(char temp_one[], int len_one, char temp_two[], int len_two) // функция сравнения чисел
{
    int i;
    char *copy_one = strdup(temp_one);
    char *copy_two = strdup(temp_two);
    if (len_one > len_two) 
        add_zero(copy_two, len_one, len_one-len_two);
    if (len_two > len_one)  
        add_zero(copy_one, len_two, len_two-len_one);
  
    i = 0;
    while ((*(copy_two + i) - '0' == *(copy_one + i) - '0') && (i < len_one))
    i++;
    if (*(copy_two + i) - '0' > *(copy_one + i) - '0') return 2;
    if (*(copy_two + i) - '0' < *(copy_one + i) - '0') return 1;
    if (i == len_one) return 0;
}

char *func_minus(char temp_one[], int len_one, char temp_two[], int len_two)
{
    int sign = 1, len, i, j;
    char *answer = strdup(temp_two);
    if (isdigit(*temp_one) && (*answer == '-') || (*temp_one == '-') && isdigit(*answer))
    {
        if (*temp_one == '-')
        {
            sign = -1;
            delChar(temp_one, '-');
            len_one--;
        }
        else
        {
            len_two--;
            delChar(answer, '-');
        }
        answer = func_plus(temp_one, len_one, answer, len_two);
        if (strlen(answer) > 1)
        {
            for (i = 0; i < strlen(answer); i++)
            {
                while (*(answer + i) - '0' == 0)
                {
                    *answer++;
                }
                break;
            }
        }
        if (sign == -1)
        {
            len_two++;
            memmove(answer + 1, answer, len_two + 1);
            memset(answer, '-', 1); 
        }
        return (answer);
    }
 
    if ((*temp_one == '-') && (*answer == '-'))
    {
        sign = -1;
        len_one--;
        delChar(temp_one, '-');
        len_two--;
        delChar(answer, '-');
    }
    len = len_one;
    int result = func_compar(temp_one, len_one, answer, len_two);
    if (result == 2)
    {
        char *copy2 = strdup(answer);
        add_zero(temp_one, len_one, len_two - len_one);
        char *copy = strdup(temp_one);
        temp_one = copy2;
        answer = copy;  
        len = len_two;
    }
    else add_zero(answer, len_two, len_one - len_two);
    for (i = len - 1; i >= 0; i--)
    {
        if (*(temp_one + i) < *(answer + i))
        {
            *(answer + i) = (*(temp_one + i) + 10 - *(answer + i) + '0');
            int x = 1;
            if (*(temp_one + i - x) - '0' == 0)
            {
                while (*(temp_one + i - x) - '0' == 0)
                {
                    x++; 
                }
                for (j = 1; j < x; j++)
                *(temp_one + i - j) = '9'; 
            }
            *(temp_one + i - x) = ((*(temp_one + i - x) -'0') - 1) + '0'; 
        }
        else
        {
            *(answer + i) = (*(temp_one + i) - *(answer + i)) + '0';
        }
    }
    if (strlen(answer) > 1)
    {
        for (i = 0; i < strlen(answer); i++)
        {
            while ((*(answer + i) - '0' == 0) && (strlen(answer) > 1))
            {
                *answer++;
            }
            break;
        }
    }
    if (result == 1)
    {
        if (sign == -1)
        {
            memmove(answer + 1, answer, len_two + 1);
            memset(answer, '-', 1); 
        }
    }
    if (result == 2)
    {
        if (sign == 1)
        {
            memmove(answer + 1, answer, len_two + 1);
            memset(answer, '-', 1); 
        }
    }
    return(answer);
}

char *func_plus(char temp_one[], int len_one, char temp_two[], int len_two) // предлагаю считать все в 10
{                                                   /* 0-ое значение отводим под знак */
    int rem = 0, i, len, sign = 1;//, sign2;
    int result;
    char *answer = strdup(temp_two);

    if ((*temp_one == '-') && isdigit(*answer) || isdigit(*temp_one) && (*answer == '-'))
    {
        if (*temp_one == '-')
        {
            sign = -1;
            delChar(temp_one, '-');
            len_one--;
        }
        else
        {
            len_two--;
            delChar(answer, '-');
        }
        answer = func_minus(temp_one, len_one, answer, len_two);
        for (i = 0; i < strlen(answer); i++)
        {
            while (*(answer + i) - '0' == 0)
            {
                *answer++;
            }
            break;
        }
        if (*answer == '-')
        return(answer);
        if (isdigit(*answer) && sign == -1)
        {
            memmove(answer + 1, answer, len_two + 1);
            memset(answer, '-', 1); 
            return(answer);
        }
        else return(answer);
    }

    if ((*temp_one == '-') && (*answer == '-'))
    {
        sign = -1;
        len_one--; 
        delChar(temp_one, '-');
        len_two--;
        delChar(answer, '-');
    }

    if (len_two < len_one) 
    {
        char *copy = strdup(temp_one);
        answer = add_zero(answer, len_two, len_one-len_two);
        len = len_one;
        temp_one = strdup(copy);
    }  
    else if (len_two > len_one)
    {
        char *copy = strdup(answer);
        len = len_two;
        temp_one =  add_zero(temp_one, len_one, len_two-len_one);
        answer = strdup(copy);
    }
    else len = len_one;

    for (i = len - 1; i >= 0; i--)
    {
        result = ((*(temp_one + i) - '0'  + *(answer + i) - '0') + rem);
        if (result >= 10)
        {
            rem = 0;
            while(result >= 10)
            {
                result -= 10;
                rem++;
                *(answer + i) = result + '0'; /* записываем результат во второе число, потом же с ним работать */
            }
        }
        else 
        {
            *(answer + i) = (*(temp_one + i) - '0' + *(answer + i) - '0' + rem) + '0';
            rem = 0;
        }
    }
    if (rem != 0)
    {
        memmove(answer + 1, answer, len);
        memset(answer, (rem + '0'), 1);
    }
    for (i = 0; i < strlen(answer); i++)
    {
        while (*(answer + i) - '0' == 0)
        {
            *answer++;
        }
        break;
    }
    if ((sign == 1))
        return (answer); /* возвращаем указатель на второе число*/
    else 
    {
        len++;
        memmove(answer + 1, answer, len);
        memset(answer, '-', 1);
        return (answer); /* возвращаем указатель на второе число*/
    }
}

int get_numbers(char str[], char *numbers[], char cbase[]) // добавила в передачу функции основание сс
{
    int i = 0;
    char *res = strtok(str, ",");
    while (res)
    {
        numbers[i++] = trans_from_radix_to_dec(res, strlen(res), atoi(cbase));
        res = strtok (NULL, ",");
    }
    return i;
}

char *func_multi(char temp_one[], int len_one, char temp_two[], int len_two)
{
    char *answer = strdup(temp_two);
    int sign = 1;
    if ((*temp_one == '-') && isdigit(*answer) || isdigit(*temp_one) && (*answer == '-'))
    {
        sign = -1;
        if (*temp_one == '-')
        {
            delChar(temp_one, '-');
            len_one--;
        }
        else
        {
            len_two--;
            delChar(answer, '-');
        }
    }
    int len = len_one + len_two, i, j, digit;  // количество разрядов результата
    int result[len+1]; // выделяем память для результата
    int index = 0;      // индекс разряда результата начиная с младшего
    for (i = 0; i <= len; i++) // обнуляем все разряды результата
        result[i] = 0;
    len--; // переходим к младшему разряду результата
    for(i = len_one-1; i >= 0; i--) // цикл для всех разрядов первого множителя
        for (j = len_two-1; j >= 0; j--) // цикл для всех разрядов второго множителя
        {
            digit = (*(temp_one + i) - '0') * (*(answer + j) - '0');
            index = i + j;          // находим текущий разряд произведения
            while (digit > 0)      // пока произведение разрядов положительно
            {
                // к текущему разряду результата добавляем остаток от деления произведения разрядов множителей на 10
                result[len - index] = result[len - index] + (digit % 10);
                if (result[len - index] > 9) // если получилась не цифра
                {
                    result[len - index] = result[len - index] % 10; // оставляем в этом разряде остаток от деления на 10
                    result[len - index + 1]++;        // добавляем 1 к предыдущему разряду
                }
                digit /= 10; // корректируем предыдущую цифру разряда результата
                index--;         // переходим к предыдущему разряду результата
                if (index > len) index = len; // не выходить за границы числа
            }
        }
    int begin = len+1; // определяем первый значащий разряд результата, не равный 0
    
    while (strlen(answer) != begin)
    {
        *(answer + i) = '0';
        i++;
    }
    for (i = begin; i > 0; i--) // все значащие разряды результата заменяем соответствующей цифрой
        *(answer + begin - i) = result[i] + '0';
    if (strlen(answer) > 1)
    {
        for (i = 0; i < strlen(answer); i++)
        {
            while (*(answer + i) - '0' == 0)
            {
                *answer++;
                // delChar((answer + i), '0');
            }
            break;
        }
    }
    if (sign == 1) return (answer);

    if (sign == -1)
    {
        memmove(answer + 1, answer, len+1);
        memset(answer, '-', 1);   
    }
    return (answer); // передаем указатель на первый значащий разряд результата.
}

char *func_mod(char temp_one[], int len_one, char temp_two[], int len_two)
{
    int sign = 1, i;
    char *answer_temp = strdup(temp_two);
    char *copy = strdup(temp_one);
    if ((*temp_one == '-') && isdigit(*answer_temp) || isdigit(*temp_one) && (*answer_temp == '-'))
    {
        sign = -1;
        if (*temp_one == '-')
        {
            delChar(temp_one, '-');
            len_one--;
        }
        else
        {
            len_two--;
            delChar(answer_temp, '-');
        }
    }
    if ((*temp_one == '-') && (*answer_temp == '-'))
    {
        delChar(temp_one, '-');
        delChar(answer_temp, '-');
        len_one--;
        len_two--;
    }
    int result = func_compar(temp_one, len_one, answer_temp, len_two);
    if (result == 2) 
    {
        if (sign == -1)
        {
            memmove(temp_one + 1, temp_one, strlen(temp_one)+1);
            memset(temp_one, '-', 1);
            return temp_one;
        }
        else return temp_one;
    }
    if (result == 0)
    {
        for (i = 0; i < len_two; i++)
            delChar(answer_temp, *(answer_temp + i));
        *answer_temp = '0';
        return answer_temp;
    }
    if (len_two == 1 && atoi(answer_temp) == 0)
    {
        printf("Error, cannot be divided by 0");
        return 0;
    }
    char *answer = func_del(temp_one, len_one, answer_temp, strlen(answer_temp));
    char *answer1 = func_multi(answer, strlen(answer), answer_temp, strlen(answer_temp));
    char *answer2 = func_minus(copy, strlen(copy), answer1, strlen(answer1));
    if (sign == -1)
    {
        memmove(answer2 + 1, answer2, strlen(answer2)+1);
        memset(answer2, '-', 1);
        return(answer2);   
    }
    return (answer2);
}

char *func_del(char temp_one[], int len_one, char temp_two[], int len_two)
{
    int sign = 1, i;
    char *answer_temp = strdup(temp_two);
    if ((*temp_one == '-') && isdigit(*answer_temp) || isdigit(*temp_one) && (*answer_temp == '-'))
    {
        sign = -1;
        if (*temp_one == '-')
        {
            delChar(temp_one, '-');
            len_one--;
        }
        else
        {
            len_two--;
            delChar(answer_temp, '-');
        }
    }
    if ((*temp_one == '-') && (*answer_temp == '-'))
    {
        delChar(temp_one, '-');
        delChar(answer_temp, '-');
        len_one--;
        len_two--;
    }
    int result = func_compar(temp_one, len_one, answer_temp, len_two);
    if (result == 2) 
    {
        for (i = 0; i < len_two; i++)
            delChar(answer_temp, *(answer_temp + i));
        *answer_temp = '0';
        return answer_temp;
    }
    if (result == 0)
    {
        for (i = 0; i < len_two; i++)
            delChar(answer_temp, *(answer_temp + i));
        *answer_temp = '1';
        if (sign == -1)
        {
            memmove(answer_temp + 1, answer_temp, strlen(answer_temp)+1);
            memset(answer_temp, '-', 1);
            return(answer_temp);   
        }
        else return (answer_temp);
        return answer_temp;
    }
    if (len_two == 1 && atoi(answer_temp) == 0)
    {
        printf("Error, cannot be divided by 0");
        return 0;
    }
    char *copy = strdup(temp_one);
    char *dividend = malloc(*copy);
    char *answer = malloc(*copy);
    while (strlen(copy) > 0)
    {
        while(func_compar(dividend, strlen(dividend), temp_two, len_two) == 2)
        {
            if (strlen(copy) == 0)
            {
                copy = "0";
                strncat(answer, copy, 1); 
                if (sign == -1)
                {
                    memmove(answer + 1, answer, strlen(answer)+1);
                    memset(answer, '-', 1);
                    return(answer);   
                }
                else return (answer);
            }
            else strncat(dividend, copy, 1);
            *copy++;
        }
        if (func_compar(dividend, strlen(dividend), temp_two, len_two) == 0)
        {
            char *ans = "1";
            strncat(answer, ans, 1);
            *dividend = '0';
        }
        else
        {
            int count = 0;
            while(func_compar(dividend, strlen(dividend), temp_two, len_two) == 1 || func_compar(dividend, strlen(dividend), temp_two, len_two) == 0)
            {
                dividend = func_minus(dividend, strlen(dividend), temp_two, len_two);
                count++;
            }
            char count1[] = {count + '0'};
            strncat(answer, count1, 1);
        }
    }
    if (sign == -1)
    {
        memmove(answer + 1, answer, strlen(answer)+1);
        memset(answer, '-', 1);
        return(answer);   
    }
    return (answer);
}

char *func_pow(char temp_one[], int len_one, char temp_two[], int len_two)
{
    int i;
    int sign = 1;
    char *answer = strdup(temp_two);
    if (*temp_one == '-')
    {
        sign = -1;
        len_one--;
        delChar(temp_one, '-');
    }
    char *copy = strdup(temp_one);
    if (*answer == '-')
    {
        printf("Error, degree must be greater than or equal to 0\n");
        return 0;
    }
    if (*answer == '0')
    {
        for (i = 0; i < len_two; i++)
            delChar(answer, *(answer + i));
        *answer = '1';
        return answer;
    }
    long int power = atoi(answer);
    for (i = 1; i < power; i++)
    {
        temp_one = func_multi(temp_one, strlen(temp_one), copy, len_one);
    }
    if (power % 2 == 0)
    {
        answer = temp_one;
        return(answer);
    }
    else if (power % 2 == 1 && sign == -1)
    {
        memmove(temp_one + 1, temp_one, strlen(temp_one)+1);
        memset(temp_one, '-', 1);  
        answer = temp_one;
        return(answer);
    }
    else 
    {
        answer = temp_one;
        return(answer);
    }
}

char *trans_from_dec_to_radix(char number[], int len, char cradix[], int len_cradix)
{
    int sign = 1, i = 0;
    if (*number == '-')
    {
        sign = -1;
        len--;
        delChar(number, '-');
    }
    char *answer = malloc(atoi(number));
    char *copy = strdup(number);
    int result = func_compar(number, len, cradix, len_cradix);
    int radix = atoi(cradix);
    if (result == 2) 
    {
        if (radix > 9 && atoi(number) > 9)
        {
            *answer = atoi(number) - 10 + 'A';
            if (sign == -1)
            {
                memmove(answer + 1, answer, len);
                memset(answer, '-', 1);
                return (answer);
            }
            else return (answer);
        }
        else if (radix > 9 && atoi(number) < 9)
        {
            answer = strdup(number);
            if (sign == -1)
            {
                memmove(answer + 1, answer, len);
                memset(answer, '-', 1);
                return (answer);
            }
            else return (answer);
        }

    }
    else
    {
        char *str = malloc(*copy);
        char *letter = malloc(*copy);

        while ((len >= 1) && (*copy != '0'))
        {
            str = func_del(copy, strlen(copy), cradix, len_cradix);
            letter = func_mod(copy, strlen(copy), cradix, len_cradix);
            if (atoi(letter) > 9)
            {
                *letter = atoi(letter) - 10 + 'A';
                strncat(answer, letter, 1); 
            }
            else
            {
                strncat(answer, letter, 1); 
            }
            copy = strdup(str);
            len = strlen(str);
        }
    }
    copy = strdup(answer);
    len = strlen(answer);
    for (i = 0; i < len; i++)
    {
        delChar(answer, *answer);
    }
    for (i = strlen(copy); i >= 0; i--)
    {
        strncat(answer, (copy+i), 1); 
    }
    if (sign == -1)
    {
        memmove(answer + 1, answer, strlen(answer)+1);
        memset(answer, '-', 1);
        return (answer);
    }
    else return (answer);
}

void func_com(size_t read, char *line, FILE *outfile)
{
    int num_count = 0, i = 0;
    char *cbase = malloc(sizeof(char)*3);
    char *numbers = malloc(sizeof(read)*20);
    char *operators = malloc(sizeof(read)*3);
    char *number[sizeof(numbers)] = {0};
    int ans = parse(line, cbase, numbers, operators); // Выделяем числа и операторы из строки
    if (ans != 0) return;
    if (cbase[0] == 0)
    {
        printf("Error, you have not entered a radix\n");
        return;
    }
    num_count = get_numbers(numbers, number, cbase);
    char *result = "0";
    if ((num_count <= 1) && isalnum(operators[0]) != 0)
    {
        printf("Sorry, you enter only one numbers for binary operators\n");
    }
    result = number[0];
    while (i < num_count-1)
    {
        switch (operators[i]) 
        {
            case '+':
                result = func_plus(result, strlen(result), number[i+1], strlen(number[i+1]));
                break;
            case '-':
                result = func_minus(result, strlen(result), number[i+1], strlen(number[i+1]));
                break;
            case '*':
                result = func_multi(result, strlen(result), number[i+1], strlen(number[i+1]));
                break;
            case '/':
                result = func_del(result, strlen(result), number[i+1], strlen(number[i+1]));
                break;
            case '^':
                result = func_pow(result, strlen(result), number[i+1], strlen(number[i+1]));
                break;
            case '%':
                result = func_mod(result, strlen(result), number[i+1], strlen(number[i+1]));
                break;
            default:
                printf("Error, incorrect operators\n");
                return;
                break;
        result = trans_from_dec_to_radix(result, strlen(result), cbase, strlen(cbase));
        }
        i++;
    }
    result = trans_from_dec_to_radix(result, strlen(result), cbase, strlen(cbase));
    if (outfile == NULL)
        printf("Answer = %s\n", result);
    else fprintf(outfile, "Answer = %s\n", result);
    if (line)
        free(line);
    
    if (cbase)
        free(cbase);
    if (numbers)
        free(numbers);
    if (operators)
        free(operators);
}

int main(int argc, char *argv[])
{
    FILE *input_file;
	FILE *output_file;
    char input_file_name[20];
	char output_file_name[20];
    char *line = NULL;
    size_t num_count = 0, i = 0, len = 0;
    size_t read;
    char yes[20];
    int flag = 1;
    if (argc == 2 && strcmp(argv[1], "-h") == 0)
    {
        printf("MODE:- HELP\n");
		printf("\nUsage: LICal [option]\n");
		printf("\t-h\t--help\t\t\tprint this help and exit\n");
		printf("\t-i\t--information\t\tprints iformation about student and exit\n");
        printf("\t-c\t--capabilities\t\ttcalculator capabilities and exit\n");
		printf("\t-o\t--out file\t\ttransfer the file with which the program will write a result \n");
		printf("\t-f\t--file\t\t\ttransfer the file with which the program will work\n\n");
        exit (EXIT_SUCCESS);
    }
    else if (argc == 2 && strcmp(argv[1], "-i") == 0)
    {
        printf("Ivanchenko Darya Vladimirovna\n");
		printf("Group M8O-110B-80\n");
		printf("Receiving teacher: Egorova Evgeniya Kirillovna\n");
		printf("Moscow Aviation Institute (National Research University). Departments 813\n");
    }
    else if (argc == 2 && strcmp(argv[1], "-c") == 0)
    {
        printf("All entered operations are performed sequentially \n");
        printf("When writing an expression, enter the number system [2:36], always in parentheses \n");
        printf("Operations: \n '+' - plus; \n '-' - minus (as well as negation, if you want to do an action with a negative number, there is no need to put parentheses); \n '*' - multiplication; \n ' / '- integer division; \n' %% '- modulo division; \n' ^ '- exponentiation\n");
        printf("Don't put the '=' sign\n");
        printf("Example: (16) AB + 123 - 5D / -89 * 9875 ^ 0 (spaces are not important)\n");
    }
    else if(argc >= 2 && strcmp(argv[1], "-f") == 0) 
    {
        if (argc == 2)
        {         
            do
            {
                printf("Input name file for read: ");
                fgets(input_file_name, 20, stdin);
                input_file_name[strlen(input_file_name) - 1] = '\0';
                if ((input_file = fopen(input_file_name, "r+")) == NULL)
                    printf("Error, file open error\n Repeat please: ");
            } while ((input_file = fopen(input_file_name, "r+")) == NULL);
        } else if (argc == 3) 
            {
            strcpy(input_file_name, argv[2]);
            if ((input_file = fopen(input_file_name, "r+")) == NULL)
            {
                printf("Error, file open error\n Repeat please: ");
                return 2;
            }
        }
        while((read = getline(&line, &len, input_file)) != -1)
        {
            func_com(read, line, NULL);
        }
        fclose(input_file);
    }
    else if(argc >= 2 && strcmp(argv[1], "-o") == 0)
    {
        if (argc == 2)
        {
            do
            {
                printf("Input name file for write: ");
                fgets(output_file_name, 20, stdin);
                output_file_name[strlen(output_file_name) - 1] = '\0';
                if ((output_file = fopen(output_file_name, "w+")) == NULL)
                    printf("Error, file open error\n Repeat please: ");
            } while ((output_file = fopen(output_file_name, "w+")) == NULL);
        }
        else if(argc == 3) 
            {
                strcpy(output_file_name,argv[2]);
                if ((output_file = fopen(output_file_name, "w+")) == NULL)
                {
                    printf("Error, file open error\n Repeat please: ");
                    return 2;
                }

            }
        while (flag)
        {
            input_file = stdin;
            printf("Would you like to enter another line?(y/n) - ");
            fgets(yes, 20, stdin);
            switch (*yes)
            {
                case 'y': // но дальше я не продвинулась, не знаю как это сделать, что бы и из файла и из консоли одни переменные отвечали
                    printf("Enter expression: ");
                    read = getline(&line, &len, input_file);
                    func_com(read, line, output_file);
                break;
                case 'n':
                    flag = 0;
                    fclose(output_file);
                    printf("Goobye!\n");
                    break;
                default:
                    printf("Error, invalid character\n");
                    fgets(yes, 20, stdin);
                    break;
            }
            memset(yes, 0, 20);
        }
    }
    else if (argc == 1)
    {
        input_file = stdin;
        while (flag)
        {
            printf("Would you like to enter another line?(y/n) - ");
            fgets(yes, 20, stdin);
            switch (*yes)
            {
                case 'y': // но дальше я не продвинулась, не знаю как это сделать, что бы и из файла и из консоли одни переменные отвечали
                    printf("Enter expression: ");
                    read = getline(&line, &len, input_file);
                    func_com(read, line, NULL);
                break;
                case 'n':
                    flag = 0;
                    printf("Goobye!\n");
                    break;
                default:
                    printf("Error, invalid character\n");
                    fgets(yes, 20, stdin);
                    break;
            }
            memset(yes, 0, 20);
        }
    }
    exit(EXIT_SUCCESS); 
}
