#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include "mychange.h"
#include "myfunc.h"

/******************************************************************
* Функция сложения для длинных чисел, прдеставленных в виде строк *
*     Все числа на входе представлены в 10-ой системе счисления   *
*******************************************************************/
char *func_plus(char temp_one[], int len_one, char temp_two[], int len_two)
{                  
    int rem = 0, i, len, sign = 1;
    int result;
    char *answer;
    if (len_one >= len_two)
        answer = calloc(pow(len_one, len_one),sizeof(char));
    else 
        answer = calloc(pow(len_two, len_two),sizeof(char));
    strcpy(answer, temp_two);

    if (((*temp_one == '-') && isdigit(*answer)) || (isdigit(*temp_one) && (*answer == '-')))
    {
        if (*temp_one == '-')
        {
            sign = -1;
            delChar(temp_one, '-');
            len_one--;
        }
        else
        {
            len_two--;
            delChar(answer, '-');
        }
        answer = func_minus(temp_one, len_one, answer, len_two);
        for (i = 0; i < strlen(answer); i++)
        {
            while (*(answer + i) - '0' == 0)
            {
                memmove(answer, answer + 1, strlen(answer) + 1);
            }
            break;
        }
        if (*answer == '-')
        return(answer);
        if (isdigit(*answer) && sign == -1)
        {
            add_elem(answer, strlen(answer), 1, '-');
            return(answer);
        }
        else return(answer);
    }

    if ((*temp_one == '-') && (*answer == '-'))
    {
        sign = -1;
        len_one--; 
        delChar(temp_one, '-');
        len_two--;
        delChar(answer, '-');
    }

    if (len_two < len_one) 
    {
        char *copy = strdup(temp_one);
        answer = add_elem(answer, len_two, len_one-len_two, '0');
        len = len_one;
        temp_one = strdup(copy);
    }  
    else if (len_two > len_one)
    {
        char *copy = strdup(answer);
        len = len_two;
        temp_one =  add_elem(temp_one, len_one, len_two-len_one, '0');
        answer = strdup(copy);
    }
    else len = len_one;

    for (i = len - 1; i >= 0; i--)
    {
        result = ((*(temp_one + i) - '0'  + *(answer + i) - '0') + rem);
        if (result >= 10)
        {
            rem = 0;
            while(result >= 10)
            {
                result -= 10;
                rem++;
                *(answer + i) = result + '0'; 
            }
        }
        else 
        {
            *(answer + i) = (*(temp_one + i) - '0' + *(answer + i) - '0' + rem) + '0';
            rem = 0;
        }
    }
    if (rem != 0)
    {
        memmove(answer + 1, answer, len);
        memset(answer, (rem + '0'), 1);
    }
    for (i = 0; i < strlen(answer); i++)
    {
        while (*(answer + i) - '0' == 0)
        {
            memmove(answer, answer + 1, strlen(answer) + 1);
        }
        break;
    }
    if ((sign == 1))
        return (answer); 
    else 
    {
        len++;
        add_elem(answer, strlen(answer), 1, '-');
        return (answer);
    }
}

/*******************************************************************
* Функция вычитания для длинных чисел, прдеставленных в виде строк *
*     Все числа на входе представлены в 10-ой системе счисления    *
********************************************************************/

char *func_minus(char temp_one[], int len_one, char temp_two[], int len_two)
{
    int sign = 1, len, i, j;
    char *answer;
    if (len_one >= len_two)
        answer = calloc(pow(len_one, len_one),sizeof(char));
    else 
        answer = calloc(pow(len_two, len_two),sizeof(char));
    strcpy(answer,temp_two);
    if ((isdigit(*temp_one) && (*answer == '-')) || ((*temp_one == '-') && isdigit(*answer)))
    {
        if (*temp_one == '-')
        {
            sign = -1;
            delChar(temp_one, '-');
            len_one--;
        }
        else
        {
            len_two--;
            delChar(answer, '-');
        }
        answer = func_plus(temp_one, len_one, answer, len_two);
        if (strlen(answer) > 1)
        {
            for (i = 0; i < strlen(answer); i++)
            {
                while (*(answer + i) - '0' == 0)
                {
                    memmove(answer, answer + 1, strlen(answer) + 1);
                }
                break;
            }
        }
        if (sign == -1)
        {
            len_two++;
            add_elem(answer, strlen(answer), 1, '-');
        }
        return (answer);
    }
 
    if ((*temp_one == '-') && (*answer == '-'))
    {
        sign = -1;
        len_one--;
        delChar(temp_one, '-');
        len_two--;
        delChar(answer, '-');
    }
    len = len_one;
    int result = func_compar(temp_one, len_one, answer, len_two);
    if (result == 2)
    {
        char *copy2 = strdup(answer);
        add_elem(temp_one, len_one, len_two - len_one, '0');
        char *copy = strdup(temp_one);
        temp_one = copy2;
        answer = copy;  
        len = len_two;
    }
    else add_elem(answer, len_two, len_one - len_two, '0');
    for (i = len - 1; i >= 0; i--)
    {
        if (*(temp_one + i) < *(answer + i))
        {
            *(answer + i) = (*(temp_one + i) + 10 - *(answer + i) + '0');
            int x = 1;
            if (*(temp_one + i - x) - '0' == 0)
            {
                while (*(temp_one + i - x) - '0' == 0)
                {
                    x++; 
                }
                for (j = 1; j < x; j++)
                *(temp_one + i - j) = '9'; 
            }
            *(temp_one + i - x) = ((*(temp_one + i - x) -'0') - 1) + '0'; 
        }
        else
        {
            *(answer + i) = (*(temp_one + i) - *(answer + i)) + '0';
        }
    }
    if (strlen(answer) > 1)
    {
        for (i = 0; i < strlen(answer); i++)
        {
            while ((*(answer + i) - '0' == 0) && (strlen(answer) > 1))
            {
                memmove(answer, answer + 1, strlen(answer) + 1);
            }
            break;
        }
    }
    if (result == 1)
    {
        if (sign == -1)
        {
            add_elem(answer, strlen(answer), 1, '-');
        }
    }
    if (result == 2)
    {
        if (sign == 1)
        {
            add_elem(answer, strlen(answer), 1, '-');
        }
    }
    return(answer);
}

/*******************************************************************
* Функция умножения для длинных чисел, прдеставленных в виде строк *
*     Все числа на входе представлены в 10-ой системе счисления    *
********************************************************************/

char *func_multi(char temp_one[], int len_one, char temp_two[], int len_two)
{
    char *answer;
    if (len_one >= len_two)
        answer = calloc(pow(len_one, len_one),sizeof(char));
    else 
        answer = calloc(pow(len_two, len_two),sizeof(char));
    strcpy(answer,temp_two);
    int sign = 1;
    if (((*temp_one == '-') && isdigit(*answer)) || (isdigit(*temp_one) && (*answer == '-')))
    {
        sign = -1;
        if (*temp_one == '-')
        {
            delChar(temp_one, '-');
            len_one--;
        }
        else
        {
            len_two--;
            delChar(answer, '-');
        }
    }
    int len = len_one + len_two, i, j, digit;
    int result[len + 1];
    int index = 0;
    for (i = 0; i <= len; i++)
        result[i] = 0;
    len--; 
    for(i = len_one - 1; i >= 0; i--) 
        for (j = len_two - 1; j >= 0; j--) 
        {
            digit = (*(temp_one + i) - '0') * (*(answer + j) - '0');
            index = i + j;         
            while (digit > 0)  
            {
                result[len - index] = result[len - index] + (digit % 10);
                if (result[len - index] > 9) 
                {
                    result[len - index] = result[len - index] % 10; 
                    result[len - index + 1]++;        
                }
                digit /= 10; 
                index--;         
                if (index > len) index = len; 
            }
        }
    int begin = len + 1; 
    
    while (strlen(answer) != begin)
    {
        *(answer + i) = '0';
        i++;
    }
    for (i = begin; i > 0; i--) 
        *(answer + begin - i) = result[i] + '0';
    if (strlen(answer) > 1)
    {
        for (i = 0; i < strlen(answer); i++)
        {
            while (*(answer + i) - '0' == 0)
            {
                memmove(answer, answer + 1, strlen(answer) + 1);
            }
            break;
        }
    }
    if (sign == 1) return (answer);

    if (sign == -1)
    {
        add_elem(answer, strlen(answer), 1, '-');
    }
    return (answer);
}

/***************************************************************************
* Функция деления по модулю для длинных чисел, прдеставленных в виде строк *
*         Все числа на входе представлены в 10-ой системе счисления        *
****************************************************************************/
char *func_mod(char temp_one[], int len_one, char temp_two[], int len_two)
{
    int sign = 1, i;


    char *answer_temp;
    char *copy;
    if (len_one >= len_two)
    {
        answer_temp = calloc(pow(len_one, len_one),sizeof(char));
        copy = calloc(pow(len_one, len_one), sizeof(char));
    }
    else 
    {
        answer_temp = calloc(pow(len_two, len_two),sizeof(char));
        copy = calloc(pow(len_two, len_two), sizeof(char));
    }
    strcpy(answer_temp,temp_two);
    strcpy(copy,temp_one);
    if (len_two == 1 && atoi(answer_temp) == 0)
    {
        printf("Error, cannot be divided by 0\n");
        if (answer_temp) free (answer_temp);
        if (copy) free (copy);
        return "!";
    }
    if (((*temp_one == '-') && isdigit(*answer_temp)) || (isdigit(*temp_one) && (*answer_temp == '-')))
    {
        sign = -1;
        if (*temp_one == '-')
        {
            delChar(temp_one, '-');
            len_one--;
        }
        else
        {
            len_two--;
            delChar(answer_temp, '-');
        }
    }
    if ((*temp_one == '-') && (*answer_temp == '-'))
    {
        delChar(temp_one, '-');
        delChar(answer_temp, '-');
        len_one--;
        len_two--;
    }
    int result = func_compar(temp_one, len_one, answer_temp, len_two);
    if (result == 2) 
    {
        if (sign == -1)
        {
            add_elem(temp_one, strlen(temp_one), 1, '-');
            return temp_one;
        }
        else return temp_one;
    }
    if (result == 0)
    {
        for (i = 0; i < len_two; i++)
            delChar(answer_temp, *(answer_temp + i));
        *answer_temp = '0';
        return answer_temp;
    }
    char *answer = func_del(temp_one, len_one, answer_temp, strlen(answer_temp));
    char *answer1 = func_multi(answer, strlen(answer), answer_temp, strlen(answer_temp));
    char *answer2 = func_minus(copy, strlen(copy), answer1, strlen(answer1));
    if (answer_temp) free(answer_temp);
    if (copy) free (copy);
    if (sign == -1)
    {
        add_elem(answer2, strlen(answer2), 1, '-');
        return(answer2);   
    }
    return (answer2);
}

/********************************************************************************
* Функция целочисленного деления для длинных чисел, прдеставленных в виде строк *
*           Все числа на входе представлены в 10-ой системе счисления           *
*********************************************************************************/

char *func_del(char temp_one[], int len_one, char temp_two[], int len_two)
{
    int sign = 1, i;
    char *answer_temp;
    if (len_one >= len_two)
        answer_temp = calloc(pow(len_one, len_one),sizeof(char));
    else 
        answer_temp = calloc(pow(len_two, len_two),sizeof(char));
    strcpy(answer_temp,temp_two);
    char *ans;
    if (len_two == 1 && atoi(answer_temp) == 0)
    {
        printf("Error, cannot be divided by 0\n");
        if (answer_temp) free (answer_temp);
        return "!";
    }
    if (((*temp_one == '-') && isdigit(*answer_temp)) || (isdigit(*temp_one) && (*answer_temp == '-')))
    {
        sign = -1;
        if (*temp_one == '-')
        {
            delChar(temp_one, '-');
            len_one--;
        }
        else
        {
            len_two--;
            delChar(answer_temp, '-');
        }
    }
    if ((*temp_one == '-') && (*answer_temp == '-'))
    {
        delChar(temp_one, '-');
        delChar(answer_temp, '-');
        len_one--;
        len_two--;
    }
    int result = func_compar(temp_one, len_one, answer_temp, len_two);
    if (result == 2) 
    {
        for (i = 0; i < len_two; i++)
            delChar(answer_temp, *(answer_temp + i));
        *answer_temp = '0';
        return answer_temp;
    }
    if (result == 0)
    {
        for (i = 0; i < len_two; i++)
            delChar(answer_temp, *(answer_temp + i));
        *answer_temp = '1';
        if (sign == -1)
        {
            add_elem(answer_temp, strlen(answer_temp), 1, '-');
            return(answer_temp);   
        }
        else return (answer_temp);
        return answer_temp;
    }

    char *copy = strdup(temp_one);
    char *dividend = malloc(*copy);
    char *answer = malloc(*copy);
    i = 0;
    while (strlen(copy) > 0)
    {
        while(func_compar(dividend, strlen(dividend), answer_temp, len_two) == 2)
        {
            if (i)
            {
                ans = "0";
                strncat(answer, ans, 1);
            }
            if (strlen(copy) == 0)
            {
                for (i = 0; i < strlen(answer); i++)
                    {
                        while (*(answer + i) - '0' == 0)
                        {
                            memmove(answer, answer + 1, strlen(answer) + 1);
                        }
                        break;
                    }
                if (dividend) free(dividend);
                if (copy) free (copy);
                if (sign == -1)
                {
                    add_elem(answer, strlen(answer), 1, '-');
                    return(answer);   
                }
                else return (answer);
            }
            else strncat(dividend, copy, 1);
            memmove(copy, copy + 1, strlen(copy) + 1);
            i++;
        }
        if (func_compar(dividend, strlen(dividend), answer_temp, len_two) == 0)
        {
            ans = "1";
            strncat(answer, ans, 1);
            *dividend = '0';
            i = 0;
        }
        else
        {
            int count = 0;
            while(func_compar(dividend, strlen(dividend), answer_temp, strlen(answer_temp)) == 1 || func_compar(dividend, strlen(dividend), answer_temp, strlen(answer_temp)) == 0)
            {
                dividend = func_minus(dividend, strlen(dividend), answer_temp, len_two);
                count++;
            }
            char count1[] = {count + '0'};
            strncat(answer, count1, 1);
            i = 0;
        }
    }
    if (dividend) free(dividend);
    if (copy) free (copy);
    if (answer_temp) free (answer_temp);
    for (i = 0; i < strlen(answer); i++)
    {
        while (*(answer + i) - '0' == 0)
        {
            memmove(answer, answer + 1, strlen(answer) + 1);
        }
        break;
    }
    if (sign == -1)
    {
        add_elem(answer, strlen(answer), 1, '-');
        return(answer);   
    }
    return (answer);
}

/******************************************************************************
* Функция возведения в степень для длинных чисел, прдеставленных в виде строк *
*           Все числа на входе представлены в 10-ой системе счисления         *
*********************************************************************************/

char *func_pow(char temp_one[], int len_one, char temp_two[], int len_two)
{
    int i;
    int sign = 1;
    char *answer;
    if (len_one >= len_two)
        answer = calloc(pow(len_one, len_one),sizeof(char));
    else 
        answer = calloc(pow(len_two, len_two),sizeof(char));
    strcpy(answer,temp_two);
    if (*temp_one == '-')
    {
        sign = -1;
        len_one--;
        delChar(temp_one, '-');
    }
    char *copy = strdup(temp_one);
    if (*answer == '-')
    {
        printf("Error, degree must be greater than or equal to 0\n");
        if (answer) free (answer);
        return "!";
    }
    if (*answer == '0')
    {
        for (i = 0; i < len_two; i++)
            delChar(answer, *(answer + i));
        *answer = '1';
        return answer;
    }
    long int power = atoi(answer);
    for (i = 1; i < power; i++)
    {
        copy = func_multi(copy, strlen(copy), temp_one, len_one);
    }
    if (power % 2 == 0)
    {
        answer = copy;
        return(answer);
    }
    else if (power % 2 == 1 && sign == -1)
    {
        add_elem(copy, strlen(answer), 1, '-'); 
        answer = copy;
        return(answer);
    }
    else 
    {
        answer = copy;
        return(answer);
    }
}

/*******************************************************************
* Функция сравнения для длинных чисел, прдеставленных в виде строк *
*     Все числа на входе представлены в 10-ой системе счисления    *
*******************************************************************/

int func_compar(char temp_one[], int len_one, char temp_two[], int len_two)
{
    int i,r;
    char *copy_one = calloc(strlen(temp_one),sizeof(char));
    strcpy(copy_one,temp_one);
    char *copy_two = calloc(strlen(temp_two),sizeof(char));
    strcpy(copy_two,temp_two);
    if (len_one > len_two) 
        add_elem(copy_two, len_one, len_one-len_two, '0');
    if (len_two > len_one)  
        add_elem(copy_one, len_two, len_two-len_one, '0');
  
    i = 0;
    while ((*(copy_two + i) - '0' == *(copy_one + i) - '0') && (i < len_one))
        i++;
    if (*(copy_two + i) - '0' > *(copy_one + i) - '0')
    {
        r = 2;
        goto CLEANUP;
    }
    if (*(copy_two + i) - '0' < *(copy_one + i) - '0')
    {
        r = 1;
        goto CLEANUP;
    }
    if (i == len_one)
    {
        r = 0;
        goto CLEANUP;
    }
    r = -1;
    goto CLEANUP;
    CLEANUP:
        free(copy_one);
        free(copy_two);
        return r;
}
