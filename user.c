#include <argp.h>
#include <stdlib.h>
#include "user.h"

/*********************************************************
* Функция для общения с пользователем и передачи строки  *
**********************************************************/
int parse_opt (int key, char *arg, struct argp_state *state)
{
    switch (key)
    {
        case 'f':
            input_file = fopen(arg, "r");
            if(input_file == NULL)
            {
                printf("Input file cannot be open!\n");
                exit(1);
            }
            break;
        case 'o':
            output_file = fopen(arg, "w+");
            if(output_file == NULL)
            {
                printf("Output file cannot be open!\n");
                exit(1);
            }
            break;
        case 'c': 
            printf("All entered operations are performed sequentially; \n");
            printf("When writing an expression, enter the number system [2:36], always in parentheses; \n");
            printf("If the number system is not specified, then by default it becomes 10;\n");
            printf("Operations: \n'+' - plus; \n'-' - minus (as well as negation, if you want to do an action with a negative number, there is no need to put parentheses); \n'*' - multiplication; \n'/' - integer division; \n'%%' - modulo division; \n'^' - exponentiation;\n");
            printf("Don't put the '=' sign;\n");
            printf("Example: (16) AB + 123 - 5D / -89 * 9875 ^ 0 (spaces are not important);\n");
            printf("Input '10 + - + - 78' is wrong.\n");
            exit(0);
            break;
        case 'i': 
            printf("Ivanchenko Darya Vladimirovna\n");
            printf("Group M8O-110B-80\n");
            printf("Receiving teacher: Egorova Evgeniya Kirillovna\n");
            printf("Moscow Aviation Institute (National Research University). Departments 813\n");
            exit(0);
            break;
    }
  return 0;
}

/***********************************************
* Структура содержащая список опций программы  *
************************************************/
struct argp_option options[] =
{
    { 0, 'f', "infile",  0, "The file to read data from"},
    { 0, 'o', "outfile", 0, "The file to write data to"},
    { 0, 'c', 0, 0, "LICal usage"},
    { 0, 'i', 0, 0, "Information about student"},
    { 0 }
};

struct argp argp = { options, parse_opt };