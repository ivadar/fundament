#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mychange.h"
#include "myformat.h"
#include "myfunc.h"
#include "user.h"

int main(int argc, char *argv[])
{
    char *line = NULL;
    size_t len = 0;
    size_t read;
    char yes[3];
    int flag = 1;
    argp_parse (&argp, argc, argv, 0, 0, 0);
    if (input_file == NULL)
    {
        printf("Would you like to enter a line for calculation?(y/n) - ");
        fgets(yes, 3, stdin);    
        while (flag)
        {
            switch (*yes)
            {
                case 'y': 
                    printf("Enter expression: ");
                    read = getline(&line, &len, stdin);
                    func_com(read, line);
                break;
                case 'n':
                    flag = 0;
                    printf("Goobye!\n");
                    return 0;
                    break;
                default:
                    printf("Error, invalid character, only y or n\n");
                    fgets(yes, 3, stdin);
                    break;
            }
            memset(yes, 0, 3);
            printf("Would you like to enter another line?(y/n) - ");
            fgets(yes, 3, stdin);
        }
        return(EXIT_SUCCESS);
    }
    else
    {
        while ((read = getline(&line, &len, input_file)) != -1)
        {
            func_com(read, line);
        }
        return 0;
    }
}