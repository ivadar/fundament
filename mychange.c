#include "mychange.h"

/****************************************
* Функция удаления элементов из строки  *
*****************************************/
char *delChar(char temp[], char del)
{
    for (char *c = temp; *c; c++)
    {
        while(*c == del)
            memmove(c, c + 1, strlen(c) + 1);
        if (*c == 0) break;
    }
    return temp;
}

/****************************************
* Функция добавления элементов в строку  *
*****************************************/
char *add_elem(char temp[], int len_one, int len, char add)
{
    memmove(temp + len, temp, len_one + 1);
    memset(temp, add, len);
    return(temp);
}
