#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include "mychange.h"

int func_compar(char temp_one[], int len_one, char temp_two[], int len_two);
char *func_plus(char temp_one[], int len_one, char temp_two[], int len_two);
char *func_minus(char temp_one[], int len_one, char temp_two[], int len_two);
char *func_multi(char temp_one[], int len_one, char temp_two[], int len_two);
char *func_mod(char temp_one[], int len_one, char temp_two[], int len_two);
char *func_del(char temp_one[], int len_one, char temp_two[], int len_two);
char *func_pow(char temp_one[], int len_one, char temp_two[], int len_two);
